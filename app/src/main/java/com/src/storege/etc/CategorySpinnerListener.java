package com.src.storege.etc;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

public class CategorySpinnerListener implements AdapterView.OnItemSelectedListener {
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        Toast.makeText(parent.getContext(),
                "Selected category: " + parent.getItemAtPosition(pos).toString(),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        Toast.makeText(arg0.getContext(),
                "Category cannot be empty!",
                Toast.LENGTH_SHORT).show();
    }
}
