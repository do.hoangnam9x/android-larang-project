package com.src.storege;

import android.app.DatePickerDialog;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.src.storege.etc.CategorySpinnerListener;

import java.text.ParseException;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    Spinner category;
    TextInputEditText nameProd;
    TextInputEditText x_dimensionProd;
    TextInputEditText y_dimensionProd;
    TextInputEditText featureProd;
    TextInputEditText noteProd;
    TextInputEditText feeProd;
    TextInputEditText reporterProd;
    TextInputEditText dateProd;
    final View.OnClickListener dateListener = new View.OnClickListener() {
        final Calendar calendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(dateProd, calendar);
            }

        };

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            new DatePickerDialog(MainActivity.this, date, calendar
                    .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)).show();
        }
    };
    final View.OnClickListener checkListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            boolean validatedData = true;

            if (!isValidString(nameProd.getText().toString(), 3, 80)) {
                validatedData = false;
                nameProd.setError("Product name must be 3 - 80 characters");
            }

            if (!isNumeric(x_dimensionProd.getText().toString(), 0.5, 99999)) {
                validatedData = false;
                x_dimensionProd.setError("Product dimension must be in numeric and cannot be negative");
            }

            if (!isNumeric(y_dimensionProd.getText().toString(), 0.5, 99999)) {
                validatedData = false;
                y_dimensionProd.setError("Product dimension must be in numeric and cannot be negative");
            }

            if (!isValidString(featureProd.getText().toString(), 3, 80)) {
                validatedData = false;
                featureProd.setError("Product feature must be 3 - 80 characters");
            }

            if (!isValidString(noteProd.getText().toString(), 0, 80)) {
                validatedData = false;
                noteProd.setError("Note cannot be more than 80 characters");
            }

            if (!isNumeric(feeProd.getText().toString(), 0, 9999999)) {
                validatedData = false;
                feeProd.setError("The price must be numeric and cannot be negative nor larger than 9999999");
            }

            if (!isValidString(reporterProd.getText().toString(), 2, 80)) {
                validatedData = false;
                reporterProd.setError("Product Reporter must have at lease 2 characters and not exceed 80 characters");
            }

            if (category.getSelectedItem() == null) {
                validatedData = false;
            }

            if (!isValidString(dateProd.getText().toString(), 10, 10) && isValidDate(dateProd.getText().toString())) {
                validatedData = false;
                dateProd.setError("Product Date is in incorrect form. Please input correctly");
            }

            if (validatedData) {
                notifyUser("Data is valid to be added");
                return;
            }
            notifyUser("Data inserted is invalid, please check again");
        }
    };
    Button usernameBtn;

    public static boolean isValidDate(String inDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("d/M/y");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nameProd = findViewById(R.id.name_prod);
        x_dimensionProd = findViewById(R.id.x_dimension_prod);
        y_dimensionProd = findViewById(R.id.y_dimension_prod);
        featureProd = findViewById(R.id.features_prod);
        noteProd = findViewById(R.id.note_prod);
        feeProd = findViewById(R.id.fee_prod);
        reporterProd = findViewById(R.id.reporter_prod);
        category = findViewById(R.id.category_prod);
        dateProd = findViewById(R.id.date_prod);

        usernameBtn = findViewById(R.id.submitButton);
        usernameBtn.setOnClickListener(checkListener);

        dateProd.setOnClickListener(dateListener);

        categorySpinnerSetup();
    }

    private void updateLabel(EditText button, Calendar calendar) {
        String format = "d/m/y";
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
        button.setText(sdf.format(calendar.getTime()));
    }

    public void categorySpinnerSetup() {
        category.setOnItemSelectedListener(new CategorySpinnerListener());
    }

    private void notifyUser(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    private boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean isNumeric(String str, double minValue) {
        return isNumeric(str) && Double.parseDouble(str) >= minValue;
    }

    private boolean isNumeric(String str, double minValue, double maxValue) {
        return isNumeric(str, minValue) && Double.parseDouble(str) <= maxValue;
    }

    private boolean isValidString(String value, int minLength, int maxLength) {
        boolean maxLengthValid = maxLength == -1 || value.length() <= maxLength;
        return value != null && value.length() >= minLength && maxLengthValid;
    }
}
